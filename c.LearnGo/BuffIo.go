package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter Number")
	input, _ := reader.ReadString('\n')
	// fmt.Printf("input: %v  type: %T\n", input, input)

	aInt, err := strconv.ParseInt(strings.TrimSpace(input), 0, 64)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("aInt: %v , type: %T", aInt, aInt)
	}

}
