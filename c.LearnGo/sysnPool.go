package main

import (
	"bytes"
	"fmt"
	"sync"
)

var memPieces int
var memPool = &sync.Pool{
	New: func() any {
		// The Pool's New function should generally only return pointer
		// types, since a pointer can be put into the return interface
		// value without an allocation:
		memPieces++
		return new(bytes.Buffer)
	},
}

func main() {
	const numWorkers = 1024 * 1024
	var wg sync.WaitGroup
	wg.Add(numWorkers)
	for i := 0; i < numWorkers; i++ {
		go func() {
			mem := memPool.Get().(*bytes.Buffer)
			memPool.Put(mem)
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Println("created memory: ",memPieces)

}
