package main

import (
	"fmt"
	"math"
	"time"
)

func main() {
	var a int64 = 10
	var b float64 = 20

	// c := a + b
	// print(c)

	d := a + int64(b)
	print(d)

	// .\addDiffType.go:7:7: invalid operation: a + b (mismatched types int64 and float64)

	var n = time.Now()
	fmt.Println(n.Format("01/02/2006"))

	val := 12.5825
	val = math.Round(val*1000) / 1000
	fmt.Println(val)

}
