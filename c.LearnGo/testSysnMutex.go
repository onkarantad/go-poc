package main

import (
	"fmt"
	"sync"
	"time"
)


	var	count int
	var lock sync.Mutex
	var rwlock sync.RWMutex
	// var wg sync.WaitGroup


func main() {
	// wg.Add(5)
	go read()
	go read()
	go read()
	go read()
	go write()
	time.Sleep(time.Second*5)
	// wg.Wait()
	fmt.Println("done")
}

func read(){
	rwlock.RLock()
	defer rwlock.RUnlock()
	// defer wg.Done()
	fmt.Println("r locked")
	time.Sleep(time.Second)
	fmt.Println("read unlocked")
}
func write(){
	rwlock.Lock()
	defer rwlock.Unlock()
	// defer wg.Done()
	fmt.Println("w locked")
	time.Sleep(time.Second)
	fmt.Println("write unlocked")
}

func basic(){
	itr := 1000
	// var wg sync.WaitGroup
	// wg.Add(1000)

	for i := 0; i < itr; i++ {
		go increment()
	}
	time.Sleep(time.Second)
	fmt.Println("counter: ", count)

}

func increment() {
	lock.Lock()
	defer lock.Unlock()
	count++
}
