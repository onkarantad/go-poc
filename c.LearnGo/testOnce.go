package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var missionCompleted bool
var attempts int
var once sync.Once

func main() {
	test2()
}

func test2() {
	var wg sync.WaitGroup
	itr := 1000
	wg.Add(itr)
	for i := 0; i < itr; i++ {
		go func() {
			if foundTreasure() {
				once.Do(markCompletred)
			}
			wg.Done()
		}()
	}
	wg.Wait()

	checkMissionCompletion()
}

func test1() {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		if foundTreasure() {
			markCompletred()
		}
		wg.Done()
	}()
	wg.Wait()

	checkMissionCompletion()
}

func markCompletred() {
	attempts++
	missionCompleted = true
}

func checkMissionCompletion() {
	if missionCompleted {
		fmt.Println("mission completed")
	} else {
		fmt.Println("mission failed")
	}
	fmt.Println("attempts: ",attempts)
}

func foundTreasure() bool {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	return 0 == r.Intn(10)
}
