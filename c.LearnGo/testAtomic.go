package main

import (
	"fmt"
	"sync/atomic"
)

func main() {

	var a int64
	atomic.StoreInt64(&a,2)

	fmt.Println(a)
	a = 3

	fmt.Println(atomic.LoadInt64(&a))

}