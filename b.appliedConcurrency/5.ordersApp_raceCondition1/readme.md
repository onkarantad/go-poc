* Orders App Functionality
    * View all products and their amount of stock
    * Place a new order if it is valid and we have enough stock
    * View an existing order

* The Orders App Endpoints
    * GET/
    * GET/products
    * POST/orders
    * GET/orders/{orderld}

* Running the App Locally
    * Install Go 1.17 or higher according to official docs
    * Fetch dependencies using go get ./...
    * Run the app using go run server.go

## Race Conditions  -> cmd/simulate
    but how does it work with multiple requests at a time?
    After all, we don't expect obligations to serve one user at a time right?
* Orders Simulation
    * Start 50 goroutines that each create a random order
    * Each goroutine sends the random order to the POST /orders endpoint using the net/ http package
    * Use the sync.WaitGroup to wait for the goroutines to complete
    * Code under cmd/simutation.go
* Server-Side Goroutines
    * The net/http package will start server-side goroutines to serve our simulated
       HTTP requests. Our back-end code will then be invoked concurrently.
* Race Conditions and Goroutines
    * Race conditions occur when multiple goroutines read and write shared data without synchronisation mechanisms
    * Race conditions create inconsistent results
    * Problems often occur with a check-then-act operation
* The Go Race Detector
    * The Go toolchain has a built-in race detector
    * Add the -race flag to any go command to use it 
        -> go run -race serve.go
    * Detects race conditions when they occur
    * Prints out stack traces and conflicting accesses when a race is found