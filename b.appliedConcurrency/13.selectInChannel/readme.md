## the select statement. 
* The select statement let's a goroutine wait on multiple channel operations. 
* They can be both send and receive operations. 
* The select blocks until one of its cases can complete, then it executes that case. 
* If multiple cases are ready at the same time, it chooses one at random.