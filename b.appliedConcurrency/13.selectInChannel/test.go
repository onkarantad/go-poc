package main

import (
	"fmt"
	"time"
)

var hellos = []string{"Hello!", "Ciao!", "Hola!", "Hej!", "Salut!"}
var goodbyes = []string{"Goodbye!", "Arrivederci!", "Adios!", "Hej Hej!", "La revedere!"}

func main() {
	// test1()
	test2()
}

func test1() {
	ch1 := make(chan string, 1)
	ch2 := make(chan string, 1)

	go greet(hellos, ch1)
	go greet(goodbyes, ch2)

	time.Sleep(1 * time.Second)
	fmt.Println("Main ready!")

	// it will go in infinite loop
	// as channel will always give zero value of that closed channel
	// so it will never go to default case and keeps calling printGreeting
	for {
		select {
		case gr := <-ch1:
			printGreeting(gr)
		case gr2 := <-ch2:
			printGreeting(gr2)
		default:
			return
		}
	}
}

func test2() {
	ch1 := make(chan string,1)
	ch2 := make(chan string,1)

	go greet(hellos, ch1)
	go greet(goodbyes, ch2)

	time.Sleep(1 * time.Second)
	fmt.Println("Main ready!")

	// to fix infinite loop issue in test1 , their is no easy solution like range for single channel check
	// one way is to check the optional ok value , if channel is closed and set it to nil , which disables operation on it
	// as nil channel block on both sends and revcives
	for {
		select {
		case gr,ok := <-ch1:
			if !ok{
				ch1 = nil
				break
			}
			printGreeting(gr)
		case gr2,ok := <-ch2:
			if !ok{
				ch2 = nil
				break
			}
			printGreeting(gr2)
		default:
			return
		}
	}
}

func greet(values []string, ch chan<- string) {
	fmt.Println("Greeter ready!")
	for _, g := range values {
		ch <- g
	}
	close(ch)
	fmt.Println("Greeter completed!")
}

func printGreeting(greeting string) {
	time.Sleep(500 * time.Millisecond)
	fmt.Println("Greeting received!", greeting)
}
