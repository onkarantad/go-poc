*** handlers/handlers -> close channel only once using Sync.Once ***
*** repo/repo -> done signal channel / createOrder() , processOrders() methods  & Close function closes done chan ***

## signal channel
* A common pattern when attempting to gracefully shut down work is to 
* instead close an additional channel known as a signal channel. 
* The purpose of this channel is not to transport information but to signal that work has completed. 
* *** Its data type is the empty struct *** to take up as little memory as possible.

* Closing Channels Only Once
    * Attempting to close an already closed channel panics
    * While the done(signal) channel stops panics on sends on the input channel,
    * we need to ensure the signal channel is only closed once
    * The sync package provides sync.Once to help