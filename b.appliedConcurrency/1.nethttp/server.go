package main

import (
	"fmt"
	"net/http"
)

func main() {
	//configure path and handler function
	http.HandleFunc("/hello",Hello)
	// Listen on port 8080 and lock main
	http.ListenAndServe(":8080",nil)

}

func Hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Hello, world !!")
}