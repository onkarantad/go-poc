The net/http Package
Provides HTTP client and server implementations

Part of the standard library
Documentation at pkg.go.dev/net/http

The HTTP Package Makes Life Easier
Request routing
HTTP client internals
HTTP server internals