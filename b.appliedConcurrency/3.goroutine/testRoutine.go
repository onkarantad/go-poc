package main

import (
	"fmt"
	"time"
)

// func main() {
// 	hello()
// 	hi()
// }
// // hello World under the Hood uses goroputine
// // 1. Allocate memory for the program.
// // 2. Start a main goroutine for our program.
// // 3. Run the program on a thread.
// // 4. Execute the code.
// // 5. Shut down and clean up once the program completes.

func main() {
	go hello()
	// time.Sleep(1 * time.Second)
	hi()
}

// Starting Our First Goroutine!
// 1. The go keyword starts a new goroutine.
// 2. go f(x) starts a new goroutine running the function f(x).
// 3. f and x are evaluated in the new goroutine.
// 4. Goroutines, like threads, share the same address space.

// as above examble go hello() starts a new go routine(new thread) but it wont stop main goroutine so
// till go routine starts hello() main routine will finish hi() function execution
// if we add sleep for 1 sec it will execute both but its not good practice to add sleep
// Never Use Sleep in Production
// It does not provide any concurrency guarantees.

// * so use sync package -> it provides locks and synchronyzation

func hello() {
	fmt.Println("hello world")
}
func hi() {
	fmt.Println("hi world")
}
