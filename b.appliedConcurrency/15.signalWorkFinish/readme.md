*** /handlers/handlers -> Close method ***
*** /repo/repo -> Close method ***
*** server.go ***

* ***POST /close***
* close endpoint, 
    * which will allow us to stop taking orders without shutting down our app. 

## Gracefully Close Orders App
* Stop receiving new orders.
* Complete all existing orders.
* Allow users to use all other functionality.
* Provide good error messages on order attempts.

* reproduce panic
    * if we call close and then call to add order
    * it will panic -> send on closed channel
    * to resolve this we need to use signal channel