***db/orders***
## The sync.Map and sync Mutex -> db/orders
* The sync.map
    * Safe for concurrent use by multiple goroutines
    * Equivalent to a safe map [interface{}] interface{}
    * The zero value is empty and ready for use -> no initilization required
    * Incurs performance overhead and should only be used as necessary
* Usage of the sync Map
    * func (m *Map) Load (key interface{}) (valueinterface{}, ok bool)    //get
    * func (m *Map) Store(key, value interface{})                        // set
    * func (m *Map) Range(f func(key, value interface{}) bool)