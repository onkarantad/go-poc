## Channel Directions
* Bidirectional channel :     chan T
* Send only channel     :     chan<-T
* Receive only channel  :   <-chan T

* Allowed operations are enforced by the compiler
* Bidirectional channels are implicitly cast to unidirectional channels & viceversa is not possible

* Unidirectional Channels as Good Practice
    * Provide a clear expectation of expected usage
    * Avoid potential confusion in the future
    * Provide type safety to our programs