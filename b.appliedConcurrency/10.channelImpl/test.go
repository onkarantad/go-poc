package main

import (
	"fmt"
	"time"
)

func main() {
	unBufferedChan()
	fmt.Println("=================")
	bufferedChan()
}

func unBufferedChan() {
	// create a unbuffered channel
	ch := make(chan string)
	// start the greeter to provide a greeting
	go greet(ch)
	// sleep for a long time
	time.Sleep(3 * time.Second)
	fmt.Println("Main ready!")
	// receive greeting
	greeting := <-ch
	// sleep and print
	time.Sleep(2 * time.Second)
	fmt.Println("Greeting received!")
	fmt.Println(greeting)

}

func bufferedChan() {
	// create a buffered unchannel by passing size of array
	bch := make(chan string, 1)
	// start the greeter to provide a greeting
	go greetUniDirectionalSendOnly(bch)
	// sleep for a long time
	time.Sleep(3 * time.Second)
	fmt.Println("Main ready!")
	// receive greeting

	greetUniDirectionalReciceOnly(bch)

	// greeting := <-bch
	// // sleep and print
	// time.Sleep(2 * time.Second)
	// fmt.Println("Greeting received!")
	// fmt.Println(greeting)

}

// greet writes a greet to the given channel and then says goodbye
func greet(ch chan string) {
	fmt.Printf("Greeter ready! & Greeter waiting to send greeting...\n")
	// greet
	ch <- "Hello, world!"
	fmt.Println("Greeter completed!")
}

func greetUniDirectionalSendOnly(ch chan<- string) {
	fmt.Printf("Greeter ready! & Greeter waiting to send greeting...\n")
	// greet
	ch <- "Hello, world!"
	fmt.Println("Greeter completed!")
}
func greetUniDirectionalReciceOnly(ch <-chan string) {
	// greet
	greeting  := <-ch
		// sleep and print
	time.Sleep(2 * time.Second)
	fmt.Println("Greeting received!")
	fmt.Println(greeting)
}