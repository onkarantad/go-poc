* channels are a concurrency synchronization mechanism that is unique to Go. 
* Let's explore them in this section. Let's consider the following scenario.

* example
```
// main go routine
func main() {
    // save calc value in main function
    calc := doWork()
    // pass this value to doMoreWork
    doMoreWork(calc)
}
// the function are commucating via shared memory of main function

func doWork() int{
    // do some work
    return 15 // calculated value
}
func doMoreWork(i int) int{
    // do more work using value i
}
```

* Wouldn't it be better if we could somehow break away from sending information back to main?
 We could then simplify our code, start the goroutines and leave them to communicate results when they're ready.
 like following eg: we are not getting calc value
 ```
func main() {
 wg.Add(2)
 doWork(&wg)
 doMoreWork(&wg)
 fmt.Println("calc2: ", calc)
 wg.Wait()
}
```
* Channels are just that and more.
    * We can view channels as internal FIFO, first in, first out queues through which we can send messages or values between goroutines.
    * Goroutines send values to the channel and some other goroutines receive values from the queue. 
    * Channels are first-class citizens in Go, so they can be used without importing any extra packages.
    * There's no need to pass values to the shared context of the main function! The channel acts as a pass-through

* senderGoRoutine ---value-->  ***channel***  ---value--> reciverGoRoutine

## Channel Syntax
* The reserved keyword chan denotes a channel The channel operator is the arrow operator *** <- ***
* -> , >- , -< are invald
* Channels are first-class citizens in Go, so they can be used without importing any extra packages. 
As we saw with the wait group and Mutex, we had to import the sync package to use them. 
* Channels are denoted with the chan keyword and their operator looks like an arrow.

* Channels are associated with a data type and only the declared data type can be transported on them
* The zero value of channels *** var ch chan T -> is nil ***
* like maps and slices, channels must be initialized using the make syntax. 
* The syntax to declare a channel of type T is -> *** ch := make (chan T) ***

* Sending and receiving are the two operations that allow us to read and write to channels. 

* Given the channel denoted by the variable ch
* Sending is done with *** ch <- data; *** the arrow points into the channel as the data travels into it
* Receiving is done with *** data := <-ch ; *** the arrow points away from the channel as data travels out of it

* Sends and Receives Are Blocking
    * Code execution will stop just like we saw with the Mutex, until the send or receive is successfully completed. 
    * The definition of completion depends on the setup of the channel.

## Unbuffered Channels -> synchronous
* channel cannot hold values by themselves -> just transfrer value from one to another go routine
* Zero capacity channels which require both sender and receiver to be present to successfully complete operations
* Unbuffered channels exchange information ***synchronously***. 
* ***By default, channels are unbuffered and this is their behavior.***

## buffered channels -> asynchronous
* Unlike unbuffered channels, they have the capacity to hold a predefined amount values without losing them. 
* If a space in the underlying values array is available, the sender can send its value to the channel and complete its send operation immediately. 
* The channel then saves the value in its underlying values array, ready for future processing. 
* When a receiver arrives at the channel, the channel can immediately forward the existing value from the array. 
* The value travels back through the channel and is removed from the underlying array. 
* The receiver then completes its operation once it has received its value. 
* Because the sender and receiver complete their operations at different times, 
* buffered channels are said to support ***asynchronous*** communication 
* The ability to support both synchronous and asynchronous operations makes channels a versatile and powerful synchronization tool. All right, that's all 