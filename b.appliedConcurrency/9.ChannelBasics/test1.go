package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	var calc int

	// eg 1
	wg.Add(1)
	doWork(&wg, &calc)
	fmt.Println("calc1: ", calc)
	wg.Wait()

	wg.Add(1)
	doMoreWork(&wg, &calc)
	fmt.Println("calc1: ", calc)
	wg.Wait()

	// it's not the most efficient solution.
	// Wouldn't it be better if we could somehow break away from sending information back to main?
	// We could then simplify our code, start the goroutines and leave them to communicate results when they're ready.
	// -- like following eg: we are not getting calc value
	//func main() {
	// wg.Add(2)
	// doWork(&wg)
	// doMoreWork(&wg)
	// fmt.Println("calc2: ", calc)
	// wg.Wait()
	//}
	
	// Channels are just that and more.
}

func doWork(wg *sync.WaitGroup, calc *int) {
	defer wg.Done()
	// var i int = 15
	*calc = 15
}
func doMoreWork(wg *sync.WaitGroup, i *int) {
	defer wg.Done()
	*i = *i + 20
}
