*** models/stats ***
*** stats/result ***
*** stats/stats ***

* Worker Pools
    * A predetermined amount of workers start up
    * All workers listen for input on a shared channel
    * The shared channel is buffered
    * The same set of workers pick up multiple pieces of work

* multiple orders at the same time. 
    * The way to implement this is using worker pools. 
    * This pattern allows us to scale up processing a task in a controlled way. 
    * We don't want to start as many workers as orders in a production system, so worker pools allow scaling to a predetermined amount. 
    * *** The worker goroutines are pre-allocated and listen to the same input channel. ***
    * They continuously pick up and process work until terminated. 
    * *** The channel is buffered to the same capacity as the amount of workers *** to allow us to construct a full queue for the workers   to pick up as quickly as possible.