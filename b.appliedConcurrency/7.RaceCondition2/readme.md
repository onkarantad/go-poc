***/repo/repo_process_test***
* The Orders Process Test  -> /repo/repo_process_test
    * Create new orders repo containing 1 product with code "TEST" and stock "11"
    * Start 10 goroutines
    * Each goroutine creates an order with amount 1 of TEST product
    * Use the sync.WaitGroup to wait for the goroutines to complete
    * Assert stock as: 11 - 10 = 1

