package main

import "fmt"

func main() {
	doWork()
}

func doWork() {
	ch := make(chan string)
	go working(ch)
	fmt.Println("Main ready!")

	// as channel is closed it will return zero value of the defined datatype , in this case its blank string
	close(ch)
	// receive
	result := <-ch
	fmt.Println(result)

	// check if channel is closed
	checkIsworking(ch)

}

func working(ch chan<- string) {
	ch <- "working ..."
}
func checkIsworking(ch <-chan string) {
	isWorking, ok := <-ch

	if !ok {
		fmt.Print("channel closed")
	}

	fmt.Println(isWorking)
}
