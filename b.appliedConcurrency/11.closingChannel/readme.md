* channels using the two operations of send and receive. 
* *** But channels support one third operation the close operation. *** 
* Closing a channel signals to other Go routines that no other values will be sent on it. 
* The syntax is simple; we can clone channel ch using *** close(ch) *** 
* *** We can close bidirectional or send only channels. *** Attempting to close a receive only channel will cause compilation error

* The Behavior of Closed Channels
    * Senders panic when sending to a closed channel
    * Receivers immediately receive the zero value of the channel data type from a closed channel
