package main

import (
	"fmt"
	"time"
)

// greetings in many languages
var greetings = []string{"Hello!", "Ciao!", "Hola!", "Hej!", "Salut!"}

func main() {
	// test1()
	// test2()
	// test3()


	// // test 4 is preffered to use range on channel
	test4()
}

func test1() {
	ch := make(chan string, 1)
	go greet1(ch)
	time.Sleep(1 * time.Second)
	fmt.Println("Main ready!")
	for {
		//  at this line it will be deadlock as
		//  main go routine has no idea when greet go routine is finished
		//  and does not know when to exit from its loop
		greeting := <-ch
		time.Sleep(500 * time.Millisecond)
		fmt.Println("Greeting received!", greeting)
	}
}
func greet1(ch chan<- string) {
	fmt.Println("Greeter ready!")
	for _, g := range greetings {
		ch <- g
	}
	fmt.Println("Greeter completed!")
}

func test2() {
	ch := make(chan string, 1)
	go greet2(ch)
	time.Sleep(1 * time.Second)
	fmt.Println("Main ready!")
	for {
		// we are oing to close the channel in gree2 go routine
		// so it wont give deadlock
		// but it will return zero value and it will be infinite loop
		greeting := <-ch
		time.Sleep(500 * time.Millisecond)
		fmt.Println("Greeting received!", greeting)
	}
}
func greet2(ch chan<- string) {
	fmt.Println("Greeter ready!")
	for _, g := range greetings {
		ch <- g
	}

	// closing the channel so main will recive zero value of datatype
	close(ch)
	fmt.Println("Greeter completed!")
}

func test3() {
	ch := make(chan string, 1)
	go greet2(ch)
	time.Sleep(1 * time.Second)
	fmt.Println("Main ready!")
	for {
		// we are oing to close the channel in gree2 go routine
		// so it wont give deadlock
		// but it will return zero value and it will be infinite loop
		greeting, ok := <-ch

		// so here we will check if channel is closed and retrn
		if !ok {
			fmt.Println("channel is closed !!")
			return
		}

		time.Sleep(500 * time.Millisecond)
		fmt.Println("Greeting received!", greeting)
	}
}

func test4() {
	ch := make(chan string, 1)
	go greet2(ch)
	time.Sleep(1 * time.Second)
	fmt.Println("Main ready!")

	// ranfge is opreffered as it will auto checks if channel is closed
	for greeting := range ch {

		// greeting, ok := <-ch
		// // so here we will check if channel is closed and retrn
		// if !ok {
		// 	fmt.Println("channel is closed !!")
		// 	return
		// }

		time.Sleep(500 * time.Millisecond)
		fmt.Println("Greeting received!", greeting)
	}
}
