## The sync Mutex ***/repo/repo***
* the sync.Mutex
    * The zero value is empty and ready for use -> no initilization required
    * The Mutex is initialised unlocked using var m sync.Mutex
    * func (m *Mutex) Lock()
    * func (m *Mutex) Unlock()
* Critical Section
    * The group of instructions that read and write
    * to shared resources which need to be executed
    * atomically without interruption


* Go race detector does have its limitations when it comes to more complex race conditions, like the one we had in order processing. 
* We will explore channels in the next section and see how they might be used to solve problems like these.