package repo

import (
	"fmt"
	"math"
	"sync"

	"github.com/applied-concurrency-in-go/db"
	"github.com/applied-concurrency-in-go/models"
)

// repo holds all the dependencies required for repo operations
// sync.Mutex The zero value is empty and ready for use -> no initilization required
type repo struct {
	products *db.ProductDB
	orders   *db.OrderDB
	lock     sync.Mutex
}

// Repo is the interface we expose to outside packages
type Repo interface {
	CreateOrder(item models.Item) (*models.Order, error)
	GetAllProducts() []models.Product
	GetOrder(id string) (models.Order, error)
}

// New creates a new Order repo with the correct database dependencies
func New() (Repo, error) {
	p, err := db.NewProducts()
	if err != nil {
		return nil, err
	}
	o := repo{
		products: p,
		orders:   db.NewOrders(),
	}
	return &o, nil
}

// GetAllProducts returns all products in the system
func (r *repo) GetAllProducts() []models.Product {
	return r.products.FindAll()
}

// GetProduct returns the given order if one exists
func (r *repo) GetOrder(id string) (models.Order, error) {
	return r.orders.Find(id)
}

// CreateOrder creates a new order for the given item
func (r *repo) CreateOrder(item models.Item) (*models.Order, error) {
	if err := r.validateItem(item); err != nil {
		return nil, err
	}
	order := models.NewOrder(item)
	r.orders.Upsert(order)
	r.processOrders(&order)
	return &order, nil
}

// validateItem runs validations on a given order
func (r *repo) validateItem(item models.Item) error {
	if item.Amount < 1 {
		return fmt.Errorf("order amount must be at least 1:got %d", item.Amount)
	}
	if err := r.products.Exists(item.ProductID); err != nil {
		return fmt.Errorf("product %s does not exist", item.ProductID)
	}
	return nil
}

// where we read and write products as well as upsert orders.
// Scrolling down to the processOrders method
// No other goroutine will be able to call any of the code past this point until it gets ahold of the lock.
// Once we have access to the lock, it's really important to make sure that it is unlocked as soon as we finish.
// This prevents other goroutines from blocking and waiting for a lock that will never become available.
// We defer the unlock operation on line 73 to ensure that the Mutex is unlocked before this method returns.
func (r *repo) processOrders(order *models.Order) {
	r.lock.Lock()
	defer r.lock.Unlock()
	r.processOrder(order)
	r.orders.Upsert(*order)
	fmt.Printf("Processing order %s completed\n", order.ID)
}

// processOrder is an internal method which completes or rejects an order
func (r *repo) processOrder(order *models.Order) {
	item := order.Item
	product, err := r.products.Find(item.ProductID)
	if err != nil {
		order.Status = models.OrderStatus_Rejected
		order.Error = err.Error()
		return
	}
	if product.Stock < item.Amount {
		order.Status = models.OrderStatus_Rejected
		order.Error = fmt.Sprintf("not enough stock for product %s:got %d, want %d", item.ProductID, product.Stock, item.Amount)
		return
	}
	remainingStock := product.Stock - item.Amount
	product.Stock = remainingStock
	r.products.Upsert(product)

	total := math.Round(float64(order.Item.Amount)*product.Price*100) / 100
	order.Total = &total
	order.Complete()
}

// to lock down the repo using locks.
// Only the goroutine which has acquired the lock is able to do its work.
// The first goroutine successful in locking the repo will be the one able to complete first.
// All other goroutines are blocked waiting for the lock to be unlocked again.
// The lock will bring order to the interleaving operations that have been causing our test failures.
// The sync package is able to help us again. The Mutex is precisely the lock we are looking for.
// Same as with the wait group, the zero value of the Mutex is initialized and ready to use.
// The Mutex is initialized in an unlocked state. The Mutex is very easy to use and it exposes two methods.
// The lock method locks the Mutex and will block until the Mutex is in an unlocked state.
// The unlock does the opposite and unlocks the Mutex and allows it to be used by another goroutine.
// In general, the Mutex should cover the section of the code that is reading and writing to the shared resource we want to protect.
// This protected part of the code is known as the critical section.
