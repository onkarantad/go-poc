Parallelism
Parallel events or tasks execute simultaneously and independently. 
True parallel events require multiple CPUs.

Concurrency
Concurrent tasks or events are interleaving and can appen in any given order. 
It is a non-deterministic way of achieving multiple tasks.
it swaps task very fast they seen to happen simultaneously
Your Busy Computer
1. Runs background tasks for updates
2. Runs your operating system
3. Writes information to disk
4. Swaps between active applications

the go runtime is in charge of running and administering concurrency in our goal programs. 
The runtime sits between the goal program and the operating system, 
and is in charge of the administration of our key concurrency mechanisms, which are go routines and channels. 

The go runtime is like a middle layer that managers go routines on OS threads and maps channel communications to system calls.