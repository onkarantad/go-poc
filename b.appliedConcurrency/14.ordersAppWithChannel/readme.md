*** repo/repo ***
* we added a lock to the repo service. 
* This allowed the process orders method to only be invoked by one order at a time. 
* However, using locks and application code does have its drawbacks. 

* First, the code becomes more verbose and error prone. 
* Developers need to remember to lock and unlock the lock for the correct behavior.
* Secondly, looking at the code, it's not intuitive to understand what the lock is used for. 
* Even with good naming, it might be difficult to reason about using it. 
* Finally, *** as the lock itself is internal state refactoring and extending this code is difficult.***
* If we were to re factor the repo to use multiple services, we would need to pass a pointer to the lock, to the methods and services that might need it