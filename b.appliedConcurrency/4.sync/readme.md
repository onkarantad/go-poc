The sync.WaitGroup
* Used to wait for goroutines to finish
* Under the hood, it uses a very simple counter and an inner lock
* The zero value of the WaitGroup is ready to be used
* We initialise it with var wg sync.WaitGroup

The Methods of the sync.WaitGroup
* func(wg *WaitGroup) Add (delta int)
    The add method adds a given number to the intercounter. 
    This number should also be the number of goroutines we wish to wait for. 
    Also add panics, if the intercounter becomes negative.
* func(wg *WaitGroup) Done()
    The Done method decrements the intercounter by one and should be used when a goroutine finishes its work.
* func(wg *WaitGroup) Wait()
    Finally, the wait blocks the goroutine from which it is invoked until the counter reaches zero.

Important Note
***All of the types in the sync package should be passed by pointer to functions.***
