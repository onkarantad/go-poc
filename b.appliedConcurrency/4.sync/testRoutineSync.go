package main

import (
	"fmt"
	"sync"
)

func main() {
	var wg sync.WaitGroup
	// how many go routime we want to wait add that counter to add method
	// default value is 0
	wg.Add(1)
	// pass wg reference to the go routine function,
	// in that we will call defer wg.Done() method to tell the routine that function ended it will decrease the wait counter
	// if we dont pass the wg reference it will pass by value that is default 0 and 
	// after done method it will decrease the counter to -1 which will panic the routine
		// Important Note
		// All of the types in the sync package should be passed by pointer to functions.
	go hello(&wg)
	// wait will wait until counter reaches to 0
	wg.Wait()
	hi()
}

func hello(wg *sync.WaitGroup) {
	defer wg.Done()
	fmt.Println("hello world")
}
func hi() {
	fmt.Println("hi world")
}
