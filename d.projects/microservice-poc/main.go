package main

import (
	"encoding/json"
	"fmt"
	"go/rest/api/custom_err"
	"go/rest/api/database"
	"go/rest/api/server"
	"go/rest/api/zlog"
	"go.uber.org/zap"
)

func main() {
	zlog.InitLogger()
	zlog.Info("Starting Application")

	db, err := database.NewDbClient()
	custom_err.HandleErr(err)
	// test(db)
	srv := server.NewEchoServer(db)
	err = srv.Start()
	custom_err.HandleErr(err)
}

func test(db database.DbClient) {
	ready := db.Ready()
	fmt.Println("ready: ", ready)
	customers, err := db.GetAllCustomers("wisdom", "penatibus.et@lectusa.com")
	custom_err.HandleErr(err)
	for _, customer := range customers {
		// fmt.Printf("customer: %+v\n", customer)
		printStructBeautify(customer)
	}
}

func printStructBeautify(v interface{}) {
	data, err := json.MarshalIndent(v, "", "    ")
	if err != nil {
		zlog.Error("Failed to marshal struct", zap.Error(err))
	}
	zlog.Info(string(data))
}
