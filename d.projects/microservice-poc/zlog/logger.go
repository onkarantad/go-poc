package zlog

import (
	"go.uber.org/zap"
)

var (
	log *zap.Logger
)

// InitLogger initializes the logger with default settings
func InitLogger() {
	var err error
	log, err = zap.NewProduction()
	if err != nil {
		panic(err)
	}
	defer log.Sync()

	// cfg := zap.NewDevelopmentConfig()
    // cfg.EncoderConfig.EncodeLevel = zapcore.CapitalColorLevelEncoder
    // cfg.EncoderConfig.EncodeCaller = zapcore.ShortCallerEncoder

    // var err error
    // logger, err = cfg.Build()
    // if err != nil {
    //     panic(err)
    // }

}

// Info logs an informational message
func Info(msg string, fields ...zap.Field) {
	log.Info(msg, fields...)
}

// Error logs an error message
func Error(msg string, fields ...zap.Field) {
	log.Error(msg, fields...)
}
