package database

import (
	"fmt"
	"go/rest/api/models"
	"log"
)

func (c Client) GetAllCustomers(schema string, email string) ([]models.Customer, error) {
	var customers []models.Customer
	query := fmt.Sprintf("SELECT * FROM %s.customers where 1=1", schema)
	params := []interface{}{}

	if email != "" {
		query += " AND email = '" + email + "'"
		params = append(params, email)
	}
	fmt.Println("query: " + query)

	err := c.DB.Select(&customers, query)
	// custom_err.HandleErr(err)
	if err != nil {
		log.Fatalf("Select failed: %v\n", err)
		return nil, err
	}
	return customers, nil
}

func (c Client) GetCustomerByEmail(email string) ([]models.Customer, error) {
	var customers []models.Customer
	query := "SELECT * FROM wisdom.customers where email = $1"
	err := c.DB.Select(&customers, query, email)
	if err != nil {
		return nil, err
	}
	return customers, nil
}

func (c Client) AddCustomer(customer *models.Customer) (*models.Customer, error) {
	fmt.Println("customer: ---- ", customer)
	return customer, nil
}
