package database

import (
	"fmt"
	"go/rest/api/models"
	"go/rest/api/zlog"
	"go.uber.org/zap"
)

func (c Client) GetAllProducts(schema string, vendor_id string) ([]models.Product, error) {
	var products []models.Product
	query := fmt.Sprintf("SELECT * FROM %s.products where 1=1", schema)
	params := []interface{}{}

	if vendor_id != "" {
		query += " AND vendor_id = '" + vendor_id + "'"
		params = append(params, vendor_id)
	}
	zlog.Info("query: "+query)

	err := c.DB.Select(&products, query)
	// custom_err.HandleErr(err)
	if err != nil {
		zlog.Error("Select failed: ", zap.Error(err))
		return nil, err
	}
	return products, nil
}
