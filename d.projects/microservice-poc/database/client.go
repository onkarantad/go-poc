package database

import (
	"context"
	"fmt"
	"go/rest/api/models"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type DbClient interface {
	Ready() bool
	GetAllCustomers(schema string, email string) ([]models.Customer, error)
	GetCustomerByEmail(email string) ([]models.Customer, error)
	AddCustomer(customer *models.Customer) (*models.Customer, error)

	GetAllProducts(schema string, vendor_id string) ([]models.Product, error)
	GetAllVendors(schema string) ([]models.Vendor, error)
	GetAllServices(schema string) ([]models.Service, error)
}

type Client struct {
	DB *sqlx.DB
}

func NewDbClient() (DbClient, error) {
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%d sslmode=%s",
		"192.168.255.128", "postgres", "postgres", "postgres", 5432, "disable")

	db, err := sqlx.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}

	fmt.Println("setting limits")
	db.SetMaxOpenConns(5)
	db.SetMaxIdleConns(5)

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	client := Client{
		DB: db,
	}

	return client, nil
}

func (c Client) Ready() bool {
	var ready string
	c.DB.QueryRow("select 1 as ready").Scan(&ready)
	return ready == "1"
}
