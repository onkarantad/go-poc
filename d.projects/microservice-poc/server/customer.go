package server

import (
	"fmt"
	"go/rest/api/custom_err"
	"go/rest/api/models"
	"net/http"

	"github.com/labstack/echo/v4"
)

func (s *EchoServer) GetAllCustomers(ctx echo.Context) error {
	email := ctx.QueryParam("email")
	fmt.Println("email: " + email)
	customers, err := s.DB.GetAllCustomers(schema, email)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err)
	}
	return ctx.JSON(http.StatusOK, customers)
}

func (s *EchoServer) AddCustomer(ctx echo.Context) error {
	customer := new(models.Customer)
	err := ctx.Bind(customer)
	if err != nil {
		return ctx.JSON(http.StatusUnsupportedMediaType, err)
	}
	customer, err = s.DB.AddCustomer(customer)
	if err != nil {
		switch err.(type) {
		case *custom_err.ConflictErr:
			return ctx.JSON(http.StatusConflict, err)
		default:
			return ctx.JSON(http.StatusInternalServerError, err)
		}
	}
	return ctx.JSON(http.StatusCreated, customer)
}
