package models

type Vendor struct {
	VendorID string `db:"vendor_id"`
	Name     string `db:"name"`
	Contact  string `db:"contact"`
	Phone    string `db:"phone"`
	Email    string `db:"email"`
	Address  string `db:"address"`
}
