package models

type Product struct {
	ProductID string `db:"product_id"`
	Name      string `db:"name"`
	Price     string `db:"price"`
	VendorID  string `db:"vendor_id"`
}
