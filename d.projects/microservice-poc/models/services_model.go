package models

type Service struct {
	ServiceId string `db:"service_id"`
	Name      string `db:"name"`
	Price     string `db:"price"`
}
