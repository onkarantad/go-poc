package models

type Customer struct {
	CustomerID string `db:"customer_id" json:"customer_id"`
	FirstName  string `db:"first_name" json:"first_name"`
	LastName   string `db:"last_name" json:"last_name"`
	Email      string `db:"email" json:"email"`
	Phone      string `db:"phone" json:"phone"`
	Address    string `db:"address" json:"address"`
}
