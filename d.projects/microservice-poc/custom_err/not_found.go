package custom_err

import "fmt"

type NotFound struct {
	Entity string
	ID     string
}

func (e *NotFound) Error() string {
	return fmt.Sprintf("unable to fing %s with id %s",e.Entity,e.ID)
}