package custom_err

type ConflictErr struct{}

func (e *ConflictErr) Error() string{
	return "attempted to create record with an existing key"
}