package database

import (
	"context"
	"errors"
	"go/rest/api/custom_err"
	"go/rest/api/models"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func (c Client) GetAllCustomers(ctx context.Context, email string) ([]models.Customer, error) {
	var customers []models.Customer
	result := c.DB.WithContext(ctx).
		Table(models.CustomerTableName()).
		Where(&models.Customer{Email: email}).Find(&customers)
	return customers, result.Error
}

func (c Client) AddCustomer(ctx context.Context, customer *models.Customer) (*models.Customer, error) {
	customer.CustomerID = uuid.NewString()

	result := c.DB.WithContext(ctx).
		Table(models.CustomerTableName()).
		Create(&customer)
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrDuplicatedKey) {
			return nil, &custom_err.ConflictErr{}
		}
		return nil, result.Error
	}
	return customer, nil
}

func (c Client) GetCustomerById(ctx context.Context, id string) (*models.Customer, error) {
	customer := &models.Customer{}

	result := c.DB.WithContext(ctx).
		Where(models.Customer{CustomerID: id}).
		First(&customer)

	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrRecordNotFound) {
			return nil, &custom_err.NotFound{}
		}
		return nil, result.Error
	}
	return customer, nil

}

func (c Client) UpdateCustomer(ctx context.Context, customer *models.Customer) (*models.Customer, error) {
	result := c.DB.WithContext(ctx).
		Model(&models.Customer{}).
		Clauses(clause.Returning{}).
		Where(&models.Customer{CustomerID: customer.CustomerID}).
		Updates(models.Customer{
			FirstName: customer.FirstName,
			LastName:  customer.LastName,
			Email:     customer.Email,
			Phone:     customer.Phone,
			Address:   customer.Address,
		})
	if result.Error != nil {
		if errors.Is(result.Error, gorm.ErrDuplicatedKey) {
			return nil, &custom_err.ConflictErr{}
		}
		return nil, result.Error
	}
	if result.RowsAffected == 0 {
		return nil, &custom_err.NotFound{Entity: "customer", ID: customer.CustomerID}
	}
	return customer, nil

}

func (c Client) DeleteCustomer(ctx context.Context, id string) error {
	return c.DB.WithContext(ctx).Delete(&models.Customer{CustomerID: id}).Error
}
