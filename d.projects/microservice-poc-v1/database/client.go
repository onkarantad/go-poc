package database

import (
	"context"
	"fmt"
	"go/rest/api/config"
	"go/rest/api/models"

	_ "github.com/lib/pq"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

type driver string

// Define the string constants
const (
	postgresDriver   driver = "postgres"
	sqlServerDeriver driver = "sqlserver"
)

// Implement the Stringer interface
func (d driver) String() string {
	return string(d)
}

type DbClient interface {
	Ready() bool
	GetAllCustomers(ctx context.Context, email string) ([]models.Customer, error)
	AddCustomer(ctx context.Context, customer *models.Customer) (*models.Customer, error)
	GetCustomerById(ctx context.Context, id string) (*models.Customer, error)
	UpdateCustomer(ctx context.Context, customer *models.Customer) (*models.Customer, error)
	DeleteCustomer(ctx context.Context, id string) error

	// GetAllProducts(schema string, vendor_id string) ([]models.Product, error)
	// GetAllVendors(schema string) ([]models.Vendor, error)
	// GetAllServices(schema string) ([]models.Service, error)
}

type Client struct {
	DB *gorm.DB
}

func NewDbClient() (DbClient, error) {
	config := config.GetConfigData()

	var db *gorm.DB
	var err error
	var dsn string

	switch config.Database.Driver {
	case postgresDriver.String():
		dsn = fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			config.Database.User,
			config.Database.Password,
			config.Database.Host,
			config.Database.Port,
			config.Database.DBName)
		db, err = gorm.Open(postgres.Open(dsn))
	case sqlServerDeriver.String():
		dsn = fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s",
			config.Database.User,
			config.Database.Password,
			config.Database.Host,
			config.Database.Port,
			config.Database.DBName)
		db, err = gorm.Open(sqlserver.Open(dsn))
	default:
		err = fmt.Errorf("unknown driver")
	}

	if err != nil {
		return nil, err
	}

	client := Client{
		DB: db,
	}

	return client, nil
}

func (c Client) Ready() bool {
	var ready string
	c.DB.Raw("select 1 as ready").Scan(&ready)
	return ready == "1"
}
