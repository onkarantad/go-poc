package database

import (
	"fmt"
	"go/rest/api/models"
	"log"
)

func (c Client) GetAllVendors(schema string) ([]models.Vendor, error) {
	var vendors []models.Vendor
	query := fmt.Sprintf("SELECT * FROM %s.vendors where 1=1", schema)
	fmt.Println("query: " + query)
	err := c.DB.Select(&vendors, query)
	if err != nil {
		log.Fatalf("Select failed: %v\n", err)
		return nil, err
	}
	return vendors, nil
}
