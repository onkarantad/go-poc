package database

import (
	"fmt"
	"go/rest/api/models"
	"log"
)

func (c Client) GetAllServices(schema string) ([]models.Service, error) {
	var services []models.Service
	query := fmt.Sprintf("SELECT * FROM %s.services where 1=1", schema)
	fmt.Println("query: " + query)
	err := c.DB.Select(&services, query)
	if err != nil {
		log.Fatalf("Select failed: %v\n", err)
		return nil, err
	}
	return services, nil
}
