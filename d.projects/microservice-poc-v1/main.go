package main

import (
	"flag"
	"go/rest/api/config"
	"go/rest/api/server"
	"go/rest/api/logger"

	"go/rest/api/custom_err"
	"go/rest/api/database"

	"go.uber.org/zap"
)

var zlog *zap.Logger

func init() {
	config_path := flag.String("config_data", "config_data.yaml", "path of configuraton file")
	flag.Parse()
	config.InitConfigData(*config_path)
	zlog = logger.GetLogger()
}

func main() {
	zlog.Info("Starting Application")
	// dsn := "postgres://postgres:postgres@192.168.255.128:5432/postgres?sslmode=disable"
	db, err := database.NewDbClient()
	if err != nil {
		zlog.Error("error creating db client", zap.Error(err))
	}
	custom_err.HandleErr(err)
	// test(db)
	srv := server.NewEchoServer(db)
	err = srv.Start()
	custom_err.HandleErr(err)
}

// func test(db database.DbClient) {
// 	ready := db.Ready()
// 	fmt.Println("ready: ", ready)
// 	customers, err := db.GetAllCustomers("wisdom", "penatibus.et@lectusa.com")
// 	custom_err.HandleErr(err)
// 	for _, customer := range customers {
// 		// fmt.Printf("customer: %+v\n", customer)
// 		printStructBeautify(customer)
// 	}
// }

// func printStructBeautify(v interface{}) {
// 	data, err := json.MarshalIndent(v, "", "    ")
// 	if err != nil {
// 		zlog.Error("Failed to marshal struct", zap.Error(err))
// 	}
// 	zlog.Info(string(data))
// }
