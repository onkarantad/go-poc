// package logger

// import (
// 	"sync"

// 	"go.uber.org/zap"
// 	"go.uber.org/zap/zapcore"
// )

// var (
// 	loggerInstance *zap.Logger
// 	once           sync.Once
// )

// func GetLogger() *zap.Logger {
// 	once.Do(func() {
// 		// Create a custom configuration
// 		config := zap.Config{
// 			Encoding:         "console", // or "console"
// 			Level:            zap.NewAtomicLevelAt(zap.InfoLevel),
// 			OutputPaths:      []string{"stdout", "out.log"}, // Log to both stdout and a file
// 			ErrorOutputPaths: []string{"stderr"},
// 			EncoderConfig: zapcore.EncoderConfig{
// 				TimeKey:        "timestamp",
// 				LevelKey:       "level",
// 				NameKey:        "logger",
// 				CallerKey:      "caller",
// 				MessageKey:     "message",
// 				StacktraceKey:  "stacktrace",
// 				LineEnding:     zapcore.DefaultLineEnding,
// 				EncodeLevel:    zapcore.LowercaseLevelEncoder, // Lowercase level names
// 				EncodeTime:     zapcore.ISO8601TimeEncoder,    // ISO8601 UTC timestamp
// 				EncodeDuration: zapcore.StringDurationEncoder,
// 				EncodeCaller:   zapcore.ShortCallerEncoder, // Short file path
// 			},
// 		}

// 		var err error
// 		loggerInstance, err = config.Build()
// 		if err != nil {
// 			panic(err)
// 		}
// 	})
// 	return loggerInstance
// }
