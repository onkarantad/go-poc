// logger/logger.go
package logger

import (
	"os"
	"sync"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var (
	logger *zap.Logger
	once   sync.Once
)

// GetLogger returns the singleton instance of the logger
func GetLogger() *zap.Logger {
	once.Do(func() {
		logger = newLogger()
	})
	return logger
}

// newLogger initializes a new instance of the logger with default configuration
func newLogger() *zap.Logger {
	logPath := "out/app.log"
	maxSizeMB := 10
	lumberjackLogger := &lumberjack.Logger{
		Filename: logPath,
		MaxSize:  maxSizeMB, // megabytes
		// MaxBackups: 3,         // maximum number of old log files to retain
		// MaxAge:     28,        // maximum number of days to retain old log files
		Compress: false, // whether to compress the old log files
	}

	zapEncoder := zapcore.NewConsoleEncoder(zapcore.EncoderConfig{
		TimeKey:        "timestamp",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		MessageKey:     "message",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.StringDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	})

	core := zapcore.NewCore(
		zapEncoder,
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(lumberjackLogger), zapcore.AddSync(zapcore.Lock(zapcore.AddSync(os.Stdout)))),
		zap.NewAtomicLevel(),
	)

	return zap.New(core, zap.AddCaller())
}

// Sync flushes any buffered log entries
func Sync() error {
	if logger == nil {
		return nil
	}
	return logger.Sync()
}
