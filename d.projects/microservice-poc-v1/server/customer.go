package server

import (
	"go/rest/api/custom_err"
	"go/rest/api/models"
	"net/http"

	"github.com/labstack/echo/v4"
)

func (s *EchoServer) GetAllCustomers(ctx echo.Context) error {
	email := ctx.QueryParam("email")
	customers, err := s.DB.GetAllCustomers(ctx.Request().Context(), email)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err)
	}
	return ctx.JSON(http.StatusOK, customers)
}

func (s *EchoServer) AddCustomer(ctx echo.Context) error {
	customer := new(models.Customer)
	err := ctx.Bind(customer)
	if err != nil {
		return ctx.JSON(http.StatusUnsupportedMediaType, err)
	}
	customer, err = s.DB.AddCustomer(ctx.Request().Context(), customer)
	if err != nil {
		switch err.(type) {
		case *custom_err.ConflictErr:
			return ctx.JSON(http.StatusConflict, err)
		default:
			return ctx.JSON(http.StatusInternalServerError, err)
		}
	}
	return ctx.JSON(http.StatusCreated, customer)
}

func (s *EchoServer) GetCustomerById(ctx echo.Context) error {
	id := ctx.Param("id")
	customers, err := s.DB.GetCustomerById(ctx.Request().Context(), id)
	if err != nil {
		switch err.(type) {
		case *custom_err.NotFound:
			return ctx.JSON(http.StatusNotFound, err)
		default:
			return ctx.JSON(http.StatusInternalServerError, err)
		}
	}
	return ctx.JSON(http.StatusOK, customers)
}

func (s *EchoServer) UpdateCustomer(ctx echo.Context) error {
	id := ctx.Param("id")
	customer := new(models.Customer)
	err := ctx.Bind(customer)
	if id != customer.CustomerID {
		return ctx.JSON(http.StatusBadRequest, "id on path doesn't match id on body")
	}
	if err != nil {
		return ctx.JSON(http.StatusUnsupportedMediaType, err)
	}
	customer, err = s.DB.UpdateCustomer(ctx.Request().Context(), customer)
	if err != nil {
		switch err.(type) {
		case *custom_err.ConflictErr:
			return ctx.JSON(http.StatusConflict, err)
		default:
			return ctx.JSON(http.StatusInternalServerError, err)
		}
	}
	return ctx.JSON(http.StatusCreated, customer)
}

func (s *EchoServer) DeleteCustomer(ctx echo.Context) error {
	id := ctx.Param("id")
	err := s.DB.DeleteCustomer(ctx.Request().Context(), id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err)
	}
	return ctx.NoContent(http.StatusResetContent)
}
