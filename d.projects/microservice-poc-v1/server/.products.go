package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

func (s *EchoServer) GetAllProdcts(ctx echo.Context) error {
	vendorId := ctx.QueryParam("vendorId")
	customers, err := s.DB.GetAllProducts(schema, vendorId)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err)
	}
	return ctx.JSON(http.StatusOK, customers)
}
