package server

import (
	"go/rest/api/database"
	"go/rest/api/models"
	"log"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Server interface {
	registerRoutes()
	Start() error
	Rediness(ctx echo.Context) error
	Liveness(ctx echo.Context) error

	GetAllCustomers(ctx echo.Context) error
	AddCustomer(ctx echo.Context) error
	GetCustomerById(ctx echo.Context) error
	UpdateCustomer(ctx echo.Context) error
	DeleteCustomer(ctx echo.Context) error

	// GetAllProdcts(ctx echo.Context) error
	// GetAllServices(ctx echo.Context) error
	// GetAllVendors(ctx echo.Context) error
}

type EchoServer struct {
	echo *echo.Echo
	DB   database.DbClient
}

func NewEchoServer(db database.DbClient) Server {
	echoServer := &EchoServer{
		echo: echo.New(),
		DB:   db,
	}
	echoServer.registerRoutes()
	return echoServer
}

func (s *EchoServer) registerRoutes() {
	s.echo.GET("/rediness", s.Rediness)
	s.echo.GET("/liveness", s.Liveness)

	cg := s.echo.Group("/customers")
	cg.GET("", s.GetAllCustomers)
	cg.POST("", s.AddCustomer)
	cg.GET("/:id", s.GetCustomerById)
	cg.PUT("/:id", s.UpdateCustomer)
	cg.DELETE("/:id", s.DeleteCustomer)

	// pg := s.echo.Group("/products")
	// pg.GET("", s.GetAllProdcts)

	// vg := s.echo.Group("/vendors")
	// vg.GET("", s.GetAllVendors)

	// sg := s.echo.Group("/services")
	// sg.GET("", s.GetAllVendors)

}

func (s *EchoServer) Start() error {
	err := s.echo.Start(":8080")
	if err != nil && err != http.ErrServerClosed {
		log.Fatalf("server shutdown occured %s ", err)
		return err
	}
	return nil
}

func (s *EchoServer) Rediness(ctx echo.Context) error {
	ready := s.DB.Ready()
	if ready {
		return ctx.JSON(http.StatusOK, models.Health{Status: "OK"})
	}
	return ctx.JSON(http.StatusInternalServerError, models.Health{Status: "Failure"})
}

func (s *EchoServer) Liveness(ctx echo.Context) error {
	return ctx.JSON(http.StatusOK, models.Health{Status: "OK"})
}
