package config

import (
	"fmt"
	"go/rest/api/custom_utils"
	"go/rest/api/logger"
	"strings"
	"sync"

	"github.com/spf13/viper"
	"go.uber.org/zap"
)

var (
	config *Config
	zlog   = logger.GetLogger()
	once   sync.Once
)

type Config struct {
	Database struct {
		Driver   string
		Host     string
		Port     int
		DBName   string
		User     string
		Password string
	}
	OdsSchema string
}

func InitConfigData(configPath string) {
	once.Do(func() {
		fileExists := custom_utils.IsFileExists(configPath)
		if !fileExists {
			err := fmt.Errorf("file not found")
			zlog.Error("", zap.Error(err))
			panic(err)
		}

		viper.SetConfigFile(configPath)
		viper.AutomaticEnv()

		replacer := strings.NewReplacer(".", "_")
		viper.SetEnvKeyReplacer(replacer)

		if err := viper.ReadInConfig(); err != nil {
			zlog.Error("Error reading config file", zap.Error(err))
		}

		// Unmarshal configuration into a struct
		if err := viper.Unmarshal(&config); err != nil {
			zlog.Error("Unable to decode into struct: ", zap.Error(err))
		}
	})

	zlog.Info("Configuration loaded successfully", zap.Any("config", config))
}

func GetConfigData() *Config {
	if config == nil {
		err := fmt.Errorf("configuration not initialized. Call InitConfigData first")
		zlog.Error("Config not initialized", zap.Error(err))
		panic(err)
	}
	return config
}
