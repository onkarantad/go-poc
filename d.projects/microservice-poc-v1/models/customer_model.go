package models

import (
	"go/rest/api/config"
)

type Customer struct {
	CustomerID string  `gorm:"primaryKey" db:"customer_id" json:"customer_id"`
	FirstName  *string `db:"first_name" json:"first_name"`
	LastName   *string `db:"last_name" json:"last_name"`
	Email      string  `db:"email" json:"email" gorm:"column:email"`
	Phone      *string `db:"phone" json:"phone"`
	Address    *string `db:"address" json:"address"`
}

func CustomerTableName() string {
	return config.GetConfigData().OdsSchema + ".customers"
}
