package models

import "go/rest/api/config"

type Product struct {
	ProductID string `db:"product_id"`
	Name      string `db:"name"`
	Price     string `db:"price"`
	VendorID  string `db:"vendor_id"`
}

func ProductTableName() string {
	return config.GetConfigData().OdsSchema + ".products"
}
