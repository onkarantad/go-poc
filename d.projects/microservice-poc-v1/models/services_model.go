package models

import "go/rest/api/config"

type Service struct {
	ServiceId string `db:"service_id"`
	Name      string `db:"name"`
	Price     string `db:"price"`
}

func ServiceTableName() string {
	return config.GetConfigData().OdsSchema + ".services"
}
