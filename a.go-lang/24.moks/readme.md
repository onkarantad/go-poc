Interfaces Wrap Structs
It's easy to substitute for interfaces with a concrete mock, 
as long as we re-implement all of the methods of the interface.

Concrete Mocks Are Suboptimal
Re-implementing entire interfaces is tedious and error prone. 
It creates a dependency between the test and the entire interface definition.

go install github.com/golang/mock/mockgen@v1.6.0
go get github.com/golang/mock/  


Mocking with golang/mock

1. Generate a mock for your interface using mockgen.
2. Bind the mock to our test runner using gomock. Controller.
3. Call EXPECT() on your mock.
4. Set up expectations and return values on your mock.

The Generate Command
* We will be using mockgen together with the generate command
* Commands are configured using special comments that must begin
with //go:generate followed by command configuration
* Run go generate ./...


1. Why is table testing a popular technique?
It allows us to test our code with a variety of inputs and scenarios.
Testing with a variety of inputs allows us to test a variety of scenarios by building a list of test cases.

2. What is the signature of Go test?
The name of the test must be in the format TestXXX and take one single testing.T parameter.
The testing.T parameter binds the test to the test runner.