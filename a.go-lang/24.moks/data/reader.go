package data

// // JSON Support in Go
// The Go standard library provides built in support for JSON encoding and decoding.
// It gives us the ability to read JSON straight into our own custom types.
// This is different from Java, which requires the use of an external library and the specific JSON object.

// The encoding/json Package
// json.Marshal encodes    			structs ==>  []byte
// json.Unmarshal method parses JSON 	[]byte ==> structs

// Only exported struct fields will be encoded/decoded

// We can use tags on field declarations to customize field names - for example,
// Name string `json:"otherName"`

// Omitting Fields
// All exported fields will be encoded. We can use tags with the name " - "
// to omit fields. For example:
// Name string `json:"-"`

import (
	"encoding/json"
	// "io/ioutil"
	"os"
)

type Response struct {
	Id          string  `json:"id"`
	Name        string  `json:"name"`
	HasBeach    bool    `json:"hasBeach"`
	HasMountain bool    `json:"hasMountain"`
	TempC       []float64 `json:"tempC"`
}

//go:generate mockgen -destination=../mocks/mock_reader.go -package=mocks linkedInLearning/tempService/data DataReader
type DataReader interface {
	ReadData() ([]Response, error)
}

type reader struct {
	path string
}

// NewReader initialises a DataReader
func NewReader() DataReader {
	return &reader{
		path: "./data/cities.json",
	}
}

// ReadData is a helper method to read the file at
// the given path and return a response array.
func (r *reader) ReadData() ([]Response, error) {
	// file, err := ioutil.ReadFile(r.path)
	byteArr, err := os.ReadFile(r.path)
	if err != nil {
		return nil, err
	}

	var data []Response
	err = json.Unmarshal(byteArr, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}
