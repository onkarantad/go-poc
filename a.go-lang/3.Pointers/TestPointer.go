package main

import "fmt"

func main() {

	// What Is a Pointer?
	// A pointer is a composite data type that stores the
	// memory address of a variable. It provides a way to
	// point to where the memory is located and find the
	// value saved at that address.

	// The * operator is known as the dereferencing operator
	// The & operator is known as the address operator

	// The syntax to declare a pointer is var name *Type
	// Forexample, var ptr *stringdeclares a string pointer
	// Given the variable a, we initialize var ptr *string = &a
	// We retrieve the value at a pointer address using value := *ptr

	// Zero Value
	// The zero value of a pointer is nil.
	// Operations on nil pointers will cause a runtime error.

	// declare pointer
	var ptr *string
	// initialise a greeting
	greeting := "Hello, world ! "
	// assign greeting address to pointer
	ptr = &greeting
	// Print out our variables
	fmt.Println("Greeting : ", greeting)
	fmt.Println("Address of greeting:", ptr)
	fmt.Println("value stored in ptr:", *ptr)

	var a = "hi"
	fmt.Println("value a : ", a)
	fmt.Println("address a : ", &a)
	fmt.Println("value at address a : ", *&a)
}

// value a :  hi
// address a :  0xc000114040
// value at address a :  hi
