Building Out Test Scenarios
Declare a testCase struct that holds the inputs and expected outputs
of the test - optionally, it can contain a test name, too

Construct a slice or map of all the test cases/scenarios that we want to
run - think of edge cases as well

Range over the test cases - run each of them in a subtest using t. Run, and
use the expected values in each testCase for assertions

External Libraries
github.com/onsi/ginkgo and github.com/stretchr/testi fy are
two testing frameworks that build on top of the standard library.