// Arrays are fixed-size collections of the same type
// different type array is not allowed

// They are declared using the syntax [n]T, where T is the type of
// the array elements and n is the size of the array

// Arrays can be declared and initialized in one step
// arr := [3]int {1,2,3}

// Zero-Value Arrays
// Unless initialized, the empty array [n]T contains n elements of the zero value of the type T
// arr := [3]int ==> by default {0,0,0}
// we can initilize some elements and leave rest as zero

// Length
// We can use the ... operator to let the compiler infer the length of the array
// arr := [...]string{"a","b","c"}
// len(arr) to get length for above its 3
// as array are fixed length this would not compile without initial set of values

package main

import "fmt"

func addCity1(city string, cities [2]string) {
	cities[1] = city
}
func addCity2(city string, cities *[2]string) {
	cities[1] = city
}

func main() {

	test := [2]int{}
	fmt.Println("test:",test)

	cities := [2]string{"london"}
	copy := cities
	fmt.Println("cities:",cities)

	fmt.Println("Cities : ", cities)
	cities[1] = "new york"
	fmt.Println("Cities : ", cities)

	// Arrays Are Value Types not the refernce
	// They are copied when assigned to a new variable or passed to functions.
	// their behaviour is kind of struct ot the interfaces
	fmt.Println("Copy1 : ", copy)
	addCity1("Miami", copy)
	fmt.Println("Copy1 : ", copy)


	fmt.Println("Copy2 : ", copy)
	addCity2("Miami", &copy)
	fmt.Println("Copy2 : ", copy)
}
