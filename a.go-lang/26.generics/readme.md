Generics was added to Go 1.18, and it's still in its infancy at the time of writing this course. 
It is much simpler than the fully fledged implementation of generics in Java. 

It allows us to create functions and data structures that take in multiple types. 
The constraints package was also added to be able to constrain type parameters. 
Types can also be constrained with custom interfaces as well. 