package main

import "fmt"

// The defer statement is applied before a function call - for example,
// defer myfunc()
// It waits function execution until the surrounding function finishes
// In the case of multiple deferred functions, they will be executed on a
// LIFO (last in, first out) basis (STACK)

// like finally block in java
// Guaranteed Run
// Deferred functions are often used for important cleanup operations.

func printMsg(id string, m string) {
	fmt.Println("id:", id, "message:", m)
}

func main() {

	// Deferred Function Parameters
	// Parameters are evaluated immediately, then used later during the deferred function invocation.
	msg := "Hello World"
	defer printMsg("defer-0", msg)
	defer printMsg("defer-1", msg)

	msg = "hiii world"
	fmt.Println("main exited ", msg)

}

// main exited  hiii world
// id: defer-1 message: Hello World
// id: defer-0 message: Hello World
