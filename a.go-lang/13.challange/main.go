package main

import (
	"com/go/poc/models"
	"com/go/poc/printer"
	"fmt"
)

func main() {
	fmt.Println("Welcome to temperature saervice !!")

	// initilize printer ad defer cleanup
	p := printer.New()
	defer p.Cleanup()
	p.CityHeader()

	// setup 3 cities
	var lon models.CityTemp
	lon = models.NewCity("London", 23,false,true)
	bcn := models.NewCity("Barcelona", 30,true,false)
	nyc := models.NewCity("New York", 28,true,false)
	ant := models.NewCity("st. anton", -3,false,true)
	asp := models.NewCity("aspen", -5,false,true)


	//print all the cities
	p.CityDetaits(lon)
	p.CityDetaits(bcn)
	p.CityDetaits(nyc)
	p.CityDetaits(ant)
	p.CityDetaits(asp)

}
