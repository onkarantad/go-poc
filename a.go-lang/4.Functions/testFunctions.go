package main

import "fmt"

// + Functions are defined using the func keyword
// + They can take in zero or more arguments and return zero or more values
//  Forexample, func add(x int, y int) int
//  takes in two integers and returns one

// Functions are exported by capitalizing their name

// Function Overloading
// + Go functions do not support method overloading
// + However, they do support variadic arguments, which is one way we
// can achieve flexibility

func add(a int, b int) int {
	return a + b
}

func main() {
	fmt.Println("SUM: ", add(5, 5))

	// annonymus function
	//  We define an anonymous function and save it to the sub variable.
	// This function only exists inside the scope of the main function
	sub := func(a int, b int) int {
		return a - b
	}

	fmt.Println("SUB: ", sub(20, 5))
}

// SUM:  10
// SUB:  15
