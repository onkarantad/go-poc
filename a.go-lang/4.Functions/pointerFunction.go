package main

import "fmt"

// Pointers and Functions
// Pointers must be used explicitly for parameters of basic types to be passed by reference
// default its pass by value

func negate(x *int) {
	neg := -*x
	*x = neg
}

func main() {
	x := 3
	fmt.Println(x) // 3
	negate(&x)
	fmt.Println(x) // -3
}

// 3
// -3
