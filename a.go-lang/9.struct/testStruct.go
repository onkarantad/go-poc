package main

import "fmt"

// what Is a Struct?  ->  equivalent Object in java (but no inheritance in struct)
// Go's structs allow us to create custom types and
// group together a collection of fields. They provide a
// way to integrate state and behavior.

// Defining Structs
// We use the type and struct keywords together to define a struct
// for example  -> type city struct
// They can be exported outside of package scope by capitalizing their name -
// for example -> type City struct

type city struct {
	name string
	temp float64
}

// Struct Creation It's standard practice to define your own constructor
// functions that encapsulate struct creation
func newCity(n string) city {
	return city{
		name: n,
		temp: 46,
	}
}

func main() {

	// shorthand initilization of struct
	city1 := city{
		name: "solapur",
		temp: 46,
	}
	fmt.Println("city1:", city1)

	// Omitted Fields We do not need to provide values to all the fields defined by a struct. The omitted fields
	// will have the zero value of their data type
	city2 := city{
		name: "mumbai",
	}
	fmt.Println("city2:", city2)

	// Structs Are Mutable
	// The dot (. ) operator allows both reading and writing to fields.
	city3 := newCity("goa")
	city3.temp = 10
	fmt.Println("city3:", city3)

}

// city1: {solapur 46}
// city2: {mumbai 0}
// city3: {goa 10}
