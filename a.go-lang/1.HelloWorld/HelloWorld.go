package main

// Hello, World!
// Explained
// The package keyword defines the code collection that the file belongs to
// In Go, executable commands must belong to package main

// Functions, types, variables, and constants are visible to all other files sharing the same package
// We can think of a package as the smallest unit of self-contained code

// file that starts with _ or . are ignored

// Main Go Tools for Development
// + go run - compiles code, creates an executable, and then runs it
// + go build - compiles code and creates an executable
// + go test -runstests, including coverage and benchmarking

// Why Are Go Binaries So Big?
// + Go binaries are statically linked standalones
// + Building a Go binary is the same - for debug or for release!
// + The build process is different from Java, where we compile to bytecode, which is then run by the JVM

// its format package we will import whole package we cant import sub method like in java
import "fmt"

func main() {
	fmt.Println("Hello World")

	var a = "Hi World"
	fmt.Println(a)

	// declare and initilize with := it will infer the type
	b := "Hi World 22"
	fmt.Println(b)

}

// Hello World
// Hi World
// Hi World 22
// hi
//  world
//  ccc
// hi
//  world
//  ccc
