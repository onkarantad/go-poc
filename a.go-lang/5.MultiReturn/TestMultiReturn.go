package main

import "fmt"

// Functions declare multiple return values by adding them to the function
// signature, enclosed in brackets
// For example, func rectangle(x int, y int) (int, int) takes
// in two integer arguments and returns two integer values

// Prefer Multiple Return Values
// Multiple return values replace tuples(map in java) in Go. They make the expected usage of a
// function explicit to callers.

func rectangle(a int, b int) (int, int) {
	area := a * b
	circum := 2 * (a + b)
	return area, circum
}

// named return values
// not recommanded for big function as it will decrease readiability
func rectangle2(a int, b int) (area int, circum int) {
	area = a * b
	circum = 2 * (a + b)
	return
}

func main() {
	a := 2
	b := 3
	area, circum := rectangle(a, b)
	fmt.Printf("area: %v\n", area)
	fmt.Printf("circum: %v\n", circum)

	// The _ operator is also known as the blank identifier
	// We can use it to ignore return values during assignment
	// In our example, we can use it instead of the ci rcumf return value

	// if declared then go comiler tell you to use it or else -> circum declared and not used
	// this kind of error will come
	// so we need to use _

	area2, _ := rectangle2(a, b)
	fmt.Printf("area2: %v\n", area2)
	// fmt.Printf("circum2: %v\n", circum2)

}

// area: 6
// circum: 10
// area2: 6
