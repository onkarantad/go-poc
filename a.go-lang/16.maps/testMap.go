// What Are Maps?
// Maps are Go's hash table implementations.
// They allow us to save key-value pairs and provide functionality to
// look up, add, and delete entries.

// Basics
// Maps are declared using the type map[KeyType]ValueType
// Only these specified types can be saved in this map.
// This is the same as we'd expect with Java hash tables.

// They are initialized using the make function together with the map type for example
// m := make(map[string]int)
// The make function takes in the map type and sets up the underlying data structures required for it.
// The make function also takes an optional size parameter.
// Maps are dynamically resized, so the size is a hint, not a limitation.

// Maps are composite data types which use pointers under the hood. Unless initialized,
// The zero value of an uninitialized map is nil - for example, var m
// map[string]int
// This is the same behavior we saw with slices,
// where explicit pointers are not required, as they themselves are references.

// create map using make
//m := make(map[string]int)
//k := "id"
//v := 101
// m[k] = v

// create map while initilization
//k := "id"
//v := 101
// m := map[string]int{k :v,}

// Missing Keys
// Reading from a map returns an optional boolean value to indicate missing keys.
// For example:
// value, ok := m[key]

package main

import "fmt"

func printValue(m map[string]int, key string) {
	val, ok := m[key]
	if !ok {
		fmt.Printf("m[\"%v\"] not found\n", key)
		return
	}
	fmt.Printf("m[\"%v\"] = %v \n", key, val)
}

func main() {
	key := "id"
	val := 101
	m := map[string]int{key: val}

	// if not found return zero value
	fmt.Println("-----> ", m[""])

	// read
	printValue(m, key)

	// update
	m["id"] = val * 2
	printValue(m, key)

	// delete
	delete(m, key)
	printValue(m, key)

}
