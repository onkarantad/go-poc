package main

import (
	"flag"
	"fmt"

	"linkedInLearning/tempService/data"
	"linkedInLearning/tempService/models"
	"linkedInLearning/tempService/printer"
)

// Usage: ./tempService [options]     -help
// Options :
// -beach
// 	Display only beach ready destinations
// -ski
// 	Display only ski ready destinations

// Implementation
// The flag package provides command-line flag parsing
// There are methods for defining flags of any type: flag.String() ,flag.Bool(),flag.Int() , etc.
// flag.Parse() parses all the command-line flags
// The flag.Usage variable defines a custom help message

func main() {
	fmt.Printf("Welcome to the LinkedIn Learning Temperature Service!\n\n")
	beachReady := flag.Bool("beach", false, "Display only beach ready dest")
	skiReady := flag.Bool("ski", false, "Display only ski ready dest")
	flag.Parse()

	// create cities
	cities, err := models.NewCities(data.NewReader())
	if err != nil {
		fmt.Println("Fatal error occurred: ", err)
		return
	}

	// initialise printer and defer cleanup
	p := printer.New()
	defer p.Cleanup()
	p.CityHeader()

	// new - CLI Impl
	cs := cities.Filter(*beachReady, *skiReady)
	for _, c := range cs {
		p.CityDetails(c)
	}

	// old -  print all the cities
	// for _, c := range cities.ListAll() {
	// 	p.CityDetails(c)
	// }

}
