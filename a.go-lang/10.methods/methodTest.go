package main

import "fmt"

type city struct {
	name  string
	tempC float64
}

func newCity(n string, t float64) city {
	return city{
		name:  n,
		tempC: t,
	}
}

// Adding Behaviors to Structs

// What is the difference between a function and a method?
// Methods are special functions that are attached to a struct type. They must be called on a struct instance.

// + Methods are functions with special receiver arguments
// + The receiver name and type appear in brackets between the func keyword and the name
// + Forexample, func (c city) tempF() float64 declaresa tempF method on the city struct
// They can be exported outside of package scope by capitalizing method name -

// Defining Methods
// The tempF method is equivalent to this tempF function! Remember,
// the method receiver is the implicit first argument of the function

// method
func (c city) tempF() float64 {
	return (c.tempC * 9 / 5) + 32
}

// function
func tempF1(c city) float64 {
	return (c.tempC * 9 / 5) + 32
}

// Method and Field Names Clash
// Field and method names cannot clash
// eg:
// type city1 struct {
// 	name  string
// 	tempC float64
// }
// // AND
// func (c city1) tempC() float64 {
// 	return (c.tempC * 9 / 5) + 32
// }

// Method Naming
// Method naming should be in MixedCaps. we can define
//  Getters and Setters using prefix Get* and Set*.

// Pointers and Methods
// Just like functionarguments, we use pointers for changes to persist outside of function scope
// using *city the chganges are persisted
func (c *city) tempF2() float64 {
	c.tempC = (c.tempC * 9 / 5) + 32
	return c.tempC
}

func main() {
	fmt.Println("hello world")

	c := newCity("goa", 0.0)
	c.tempC = 7.5
	fmt.Println("city: ", c)

	//method calling
	temp1 := c.tempF()

	//function calling
	temp2 := tempF1(c)

	fmt.Println("city: ", c.name, c.tempC, temp1, temp2)

	// before persistance
	fmt.Println("city: ", c)

	// calling pointer method for persisting data
	temp3 := c.tempF2()
	fmt.Println("city: ", c.name, c.tempC, temp1, temp2, temp3)

	// after persistance
	fmt.Println("city: ", c)

}
