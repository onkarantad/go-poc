package models

import (
	"linkedInLearning/tempService/data"
	"sort"
	"strings"
)

type cities struct {
	cityMap map[string]CityTemp
}

type Cities interface {
	// ListAll() []CityTemp
	Filter(cq CityQuery) []CityTemp
}

// NewCities initialises the Cities data structure by calling the
// ReadData method to read information from file.
func NewCities(reader data.DataReader) (Cities, error) {
	d, err := reader.ReadData()
	if err != nil {
		return nil, err
	}
	cmap := make(map[string]CityTemp)
	for _, r := range d {
		cmap[r.Id] = NewCity(r.Name, r.TempC, r.HasBeach, r.HasMountain)
	}

	return &cities{
		cityMap: cmap,
	}, nil
}

// new
// ListAll returns a slice of all the cities.
func (c cities) listAll() []CityTemp {
	var cs []CityTemp
	for _, rc := range c.cityMap {
		cs = append(cs, rc)
	}
	sortAlphabetically(cs)
	return cs
}

// sorting
func sortAlphabetically(cs []CityTemp) {
	sort.Slice(cs, func(i, j int) bool {
		return cs[i].Name() < cs[j].Name()
	})
}

// Filter process the beach and ski markers and returns the list cities.
func (c cities) Filter(cq CityQuery) []CityTemp {
	// no flags set, return all
	if !cq.Beach() && !cq.Ski() && cq.Name() == "" {
		return c.listAll()
	}
	return c.filterHelper(cq)
}

// filterHelper is a helper method that processes the values of
// beach and ski flags on the list of cities.
func (c cities) filterHelper(cq CityQuery) []CityTemp {
	var cs []CityTemp
	for _, rc := range c.cityMap {
		if matchFilter(rc, cq) {
			cs = append(cs, rc)
		}
	}
	sortAlphabetically(cs)
	return cs
}

// matchFilter returns whether the given city matches given filter parameters
func matchFilter(rc CityTemp, cq CityQuery) bool {
	if cq.Beach() && rc.BeachVacationReady(cq) {
		return true
	}
	if cq.Ski() && rc.SkiVacationReady(cq) {
		return true
	}
	if cq.Name() != "" && strings.Contains(strings.ToLower(rc.Name()), cq.Name()) {
		return true
	}

	return false
}
