temperature service.

The pattern we've used has been to leave structs package scope and export interfaces.

The pattern we've been using has been to 
Export Interfaces, Hide Structs
export interfaces which have well defined methods while keeping structs package level. 

We have also implemented constructor functions which internally create the structs but return the interface type. 
This ensures that structs fields are not modified unexpectedly and only exported interface methods are available outside the package. 

As we will shortly see using this pattern in your code will also create robust code that is easy to test.

========================

Which of the following is TRUE of Go JSON encoding and decoding?
Only exported struct fields can be encoded/decoded.
As the JSON package uses reflection, only exported fields will be visible to it.


What functionality does the flag package provide?
It provides functionality for command line flag parsing.
The flag package provides functionality for defining and parsing command line flags.

How do we sort a slice of structs?
The sort package provides the sort.Slice function.
The sort.Slice function takes in a slice of structs and a comparison function.

What does the range operator return when applied to slices and maps?
For slices, the range returns the index and value of each item. For maps, the range returns the key and value of each item.
The range returns the index and value for slices and arrays. It returns the key and value for maps.


What happens when we try to access a map with a key it does not contain?
The zero value of value type is returned.
Maps return the zero value of its value type if a key is not found, making all values safe.