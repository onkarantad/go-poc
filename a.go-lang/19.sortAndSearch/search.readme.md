* The sort.slice method implements an optimized version of quick sort with a performance of O and login.
* The function takes in a slice and a comparison function.
* It is used to sort slices of custom types.

* the empty interface will match all data types, 
* but this function will panic if it is not passed the slice.
* We only need to use the sort.slice when using custom structs.

## Sorting
* The Slice(x interface{}, less func(i, j int) bool)
* function sorts a slice using the less function
* There are three functions to sort basic types: // the sort package provides built-in functions for
    * *** sort.Ints,sort.strings and sort.Float64s ***

## Searching
* The sort package also has functions for searching. 
* The search function takes in the length of the slice to search in 
* and a function which will be true for the value searched for.
* The search function uses binary search, and will return the index of the searched value. 
* A common usage of this function is to search through already sorted structures for optimal performance. 
* The Search(n int, f func(int) bool) int 
* function finds the smallest index for which the function f is true
* There are three functions to search basic types: 
* sort.Searchlnts,sort.SearchStrings,andsort.SearchFloat64s
* check in cities.go