package models

import "fmt"

type query struct {
	beach bool
	ski   bool
	month int
	name  string
}

type CityQuery interface {
	Beach() bool
	Ski() bool
	Month() int
	Name() string
}

func NewQuery(beach bool, ski bool, month int, name string) (CityQuery, error) {
	cq := &query{
		beach: beach,
		ski:   ski,
		month: month,
		name:  name,
	}

	if err := cq.validate() ; err !=nil{
		return nil , err
	}

	return cq, nil
}

func (q query) validate() error{
	if q.month < 1 || q.month >12{
		return fmt.Errorf("must be in range [1,12]; got %v",q.month)
	}
	return nil
} 

func (q query) Beach() bool {
	return q.beach
}
func (q query) Ski() bool {
	return q.ski
}
func (q query) Month() int {
	return q.month
}
func (q query) Name() string {
	return q.name
}
