Test-Driven Development (TDD)

Basics
The testing package provides testing fundamentals

Test files are place in the same directory and must end in _test.go
Test functions must follow the signature 
func TestName(t *testing.T)
Tests are run using the 
go test ./... -v  ->  this will run all test in given directory and sub-directory


Key Methods
1. t.Run allows us to run subtests in a given test.
2. t.Errorf reports a failed test but continues execution.
3. t.Fatalf reports a failed test and stops execution.
4. t.Helper allows us to register helper functions. -> skip printing line and file information during logging
5. t.Skip allows us to mark a test as skipped.

Assertions
The testing package does not provide any assertion methods. 
There are multiple libraries that you can use, 
**TO DO LATER**

Package Testing
You should declare the package of your tests as _ test as well. This will ensure
that you are only testing the external methods of your package.