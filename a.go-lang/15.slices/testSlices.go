// What Are Slices?
// Slices are powerful wrapper references on top of arrays.
// They give us the functionality of dynamic arrays (ArrayList in java) but are not exactly equivalent.

// Creating Slices
// They are created using the syntax []T
// Slices can also be initialized in one step eg:
// c := []string {"a","b"}

// Given an array a, we can also create a slice with the syntax
// a[start:end]

// Slices can also be created using the make function eg:
// c := make([]string, len, cap)

// a := make([]string,3,4)
// make function allocates underlying array with zero vales
// len:3 and cap:4 -> {1,0,0}

// all other slices point to same underlying array {1,0,0}
// ** as its a reference

// if we declare 2nd slice with start and end it will point to same array with adjusted length but same capacity
// b := a[0,2]  -> adjusted length {1,0} -> pointing to same {1,0,0} array

// as slices dont hold any value themself if value changed on one slice then it will change in underlying array
// and change all the sclices pointing to that array

// Modifying Slices
// Slices can be modified by index directly - for example
// a[0] = 1
// Elements can also be appended to the a slice by using the append function - for example
// a = append(a, 2) -> returns new slice
// append function increases the length of slice and add element to end & return new slice
// now after append underlying array -> {1,0,0,2} -> len:4 and cap:4

//  If the underlying array is full, it will be resized to make space for the new element.
// The underlying array is resized similar to Java, and the elements are moved to a new memory block.
// This is also the reason that the append function returns a new slice,
// as the data might have moved to a completely different place during append.

package main

import "fmt"

func main() {
	test := make([]int,2,3)
	fmt.Println("test:",test)

	cities := make([]string, 1, 1)
	copy := cities
	fmt.Println("cities:",cities)

	cities[0] = "london"
	cities = append(cities, "new work")

	fmt.Printf("[%p]cities = %v , len = %v , cap = %v", &cities, cities, len(cities), cap(cities))
	fmt.Println()
	fmt.Printf("[%p]copy = %v , len = %v , cap = %v", &copy, copy, len(copy), cap(copy))
}
