Go handles concurrency particularly well. 

The two most important concurrency concepts are Goroutines and channels. 

Goroutines are a kind of lightweight thread that allows us to run tasks asynchronously. 

Channels are thread safe queues that allow us to pass values and information between Go routines. 

Let's have a look at a small concurrency example. On lines seven to 15, we have 