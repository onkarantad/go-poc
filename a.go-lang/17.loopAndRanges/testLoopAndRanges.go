// Java has a wide range of collections, iterators and enumerators.
// In Go, where the philosophy is simplicity, there are only two ways to iterate our collections.
// We can iterate Go slices and maps in two ways:
// 1. for loop
// 2. range operator

// SLICES

// cities := []string{"london","tokyo","soul"}

// // using for loop
// for i:0 ; i<len(cities); i++{
// 	print(cities[i])
// }

// // using range
// i is index city is value
// for i, city: range cities{
// 	print(city)
// }

// // if we dont need values
// for _, city: range cities{
// 	println(city)
// }

// MAPS

// m := map[string]int{
// 	"id1": 101,
// 	"id2": 102,
// 	"id3": 103
// }

// for k,v := range m {
// 	print("k:v")
// }

package main

import "fmt"

func maxtempCity(temp map[string]float64) (string,float64){
	var city string
	var maxT float64

	for c,t := range temp{
		if t > maxT{
			city = c
			maxT = t
		}
	}
	return city,maxT

}

func main(){
	m := map[string]float64{
		"mumbai": 24.5,
		"goa": 30,
		"solapur": 40,
	}

	c,t := maxtempCity(m)
	fmt.Printf("the hottest city is %v with temperatue of %v \n",c,t)

}