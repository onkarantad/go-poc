package main

import "fmt"

// There is no exception type in Go
// error handling/checking is part of the regular program flow
// Go provides the inbuilt error type - its zero value is nil
// Errors are initialized using
// errors.New("msg") or  fmt.Errorf("msg:", x)

// The zero value of a pointer is nil.

// Explicit Error Handling
// We use multiple return values, pointers, and if/else statements to handle errors.
func rectangle(a int, b int) (*int, error) {

	if a == 0 || b == 0 {
		return nil, fmt.Errorf("Zero Input", a, b)
	}

	area := a * b
	return &area, nil
}

func main() {
	a := 0
	b := 3

	// checking errors
	area, err := rectangle(a, b)
	// if err != nil {
	// 	fmt.Print("error:", err)
	// 	return
	// }

	// todo: panic and recover
	if err != nil {
		panic(err)
	}

	fmt.Print("area: ", *area)

}

// panic: Zero Input%!(EXTRA int=0, int=3)

// goroutine 1 [running]:
// main.main()
//         D:/azure/sapiens-git-onkar/GO-Lang/7.ErrorHangler/TestErrorHandle.go:35 +0x9c
// exit status 2
