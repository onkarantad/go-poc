module com/go/poc

go 1.22.3



// What does a project's go.mod file specify?
// the project's import path, dependencies and Go version
// The go.mod file specifies everything we need to run the project, as well as its import path and go version.