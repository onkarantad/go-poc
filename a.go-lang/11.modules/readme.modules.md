multi package project
modulles introduced in 1.11
modules are collecion of packages to be released and distributed together
Modules are how - Go manages dependencies
A new module is created using go mod init packageName
for example
--> go mod init com/go/poc
The go mod command should be run in the project root directory


External Modules
• Modules are distributed from Git repositories
• We use the repository path as a way to reference external modules we want to use
• The go get command will download any missing dependencies specified in the go. mod file


One Package per Directory
Remember that we can only define one package per directory. We will need to
structure our project accordingly.


Mixing Export Definitions
A method or field cannot be exported if its corresponding struct is not exported.
The opposite is possible.