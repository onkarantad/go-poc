package main

import (
	"fmt"
	"com/go/poc/models"
	"com/go/poc/printer"
)

func main() {
	fmt.Println("Welcome to temperature saervice !!")

	// initilize printer ad defer cleanup
	p := printer.New()
	defer p.Cleanup()
	p.CityHeader()

	// setup 3 cities
	var lon *models.City
	lon = models.NewCity("London")
	lon.TempC = 7.5
	ams := models.NewCity("Amsterdam")
	ams.TempC = 11
	nyc := models.NewCity("New York")
	nyc.TempC = -3

	//print all the cities
	p.CityDetaits(lon)
	p.CityDetaits(ams)
	p.CityDetaits(nyc)

}
