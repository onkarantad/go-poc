package main

import "fmt"

// Basic types: numbers, strings, and booleans
// Composite types: arrays, structs, pointers, functions, interfaces, slices, maps, and channels

// Integer Type Aliases
//  byte is an alias foruint8, and
//  rune is an alias for int32.

// Floating Point Types
// Go has two floating point types: float32 and float64
// f := 123.45 will be allocated to type float64

// Booleans
// + The type boo stores boolean values: true and false
// + Boolean operators are && (AND), || (OR),and! (negation)
// + The operators && and | | follow short-circuiting rules
// » Forexample, in the expression vl && v2,if vl is false,then v2 is not evaluated

// variable are defined with default 0 values and blank sting for string value
var (
	v0 int
	v1 float64
	v2 bool
	v3 string
)

const aConst string = "hii"

func main() {

	// Strings
	var c = "hi \n world \n ccc"
	fmt.Println(c)

	//  ` with this multi line string we can define`
	var d = `hi
 world
 ccc`
	fmt.Println(d)

	// Exporting Names
	// + A name can be exported by using a capital letter, making it available outside of package scope
	var Greeting = "Hello, World!"
	fmt.Println(Greeting)

	// + The Java equivalent of this syntax is:
	// public String greeting = "Hello, World!"

	// fmt.Print("Greeting:", Greeting)

	fmt.Printf("v0 type:%T | value: %v\n", v0, v0)
	fmt.Printf("v1 type:%T | value: %v\n", v1, v1)
	fmt.Printf("v2 type:%T | value: %v\n", v2, v2)
	fmt.Printf("v3 type:%T | value: %v\n", v3, v3)

}

// v0 type:int | value: 0
// v1 type:float64 | value: 0
// v2 type:bool | value: false
// v3 type:string | value:
