package models

type city struct {
	name  string
	tempC float64
}

type CityTemp interface {
	Name() string
	TempC() float64
	TempF() float64
}

// How does a struct implement an interface?
// by implementing all the methods that the inteface defines
// The compiler will automatically implement an interface on the struct once it implements all the interface methods.

// NewCity creates a new City instance with the given name
func NewCity(n string, t float64) CityTemp {
	return &city{
		name:  n,
		tempC: t,
	}
}

func (c city) Name() string {
	return c.name
}
func (c city) TempC() float64 {
	return c.tempC
}
func (c city) TempF() float64 {
	return (c.tempC * 9 / 5) + 32
}
