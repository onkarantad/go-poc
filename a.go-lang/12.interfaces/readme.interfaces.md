1. We already know that structs don't support inheritance, so let's now have a look at how we can achieve polymorphism with interfaces.

2. We can think of interfaces as named collections of methods or behaviors. 

3. Unlike Java, Go does not have an implements keyword. 

4. Structs simply implement interfaces by implementing the methods that they define.

5. The compiler will check and enforce interface types where needed. 

6. Being able to define and implement new interfaces without needing to modify existing code gives Go great flexibility and makes interfaces lightweight.

7. Similar to structs, we define interfaces using the type and interface keywords separated by the name of the interface. 

8. For example, the syntax
    type CityTemp interface
defines an interface named CityTemp, short for CityTemperature. 

9. Structs can implement multiple interfaces.
This is one of the reasons why they are very powerful polymorphism mechanisms.

10. A struct can also implement more methods than its interfaces. 
 
11. The empty interface{} does not define any methods. Therefore, all structs satisfy it. 
 
12. The zero value of interfaces is nil. 
 Just like we did with the error type, we should check interface values for nil to see if they have been set. 
 
**Finally, interfaces use pointers under the hood so unlike structs, they don't need to be explicitly passed by a pointer**
 
13. A good usage of interfaces is to use them to expose well-defined behaviors outside of their package. 
  They have easy-to-read compiler-enforced method signatures, making them ideal for this purpose. 
  
14. **It is therefore common practice to export the interface and leave the struct unexported.**
  In this way, the struct fields and methods are not accessible for unexpected mutations. 
  
  I have changed our temperature service to use interfaces to wrap around our city instances. 
  The city struct and its fields are now unexported on lines three to six.
  
  Other packages will need to use the CityTemp interface, which is declared on line nine. 
  
  This interface declares three methods: name, TempC or TemperatureCelsius and TempF or TemperatureFahrenheit. 