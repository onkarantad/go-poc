package main

import "fmt"

// Unlike Java, it does not require round brackets around its conditionals

func isEven(x int) {
	if x%2 == 0 {
		fmt.Println(x, "is even")
	} else {
		fmt.Println(x, " is odd")
	}
}

// remove else by using return
func isEven2(x int) {
	if x%2 == 0 {
		fmt.Println(x, "is even2")
		return
	}
	fmt.Println(x, " is odd2")
}

// shorthand declaration for variable
// Shorthand Variables
// The scope of the variable r is only inside the if-else statement block
func isEven3(x int) {
	if r := x % 2; r == 0 {
		fmt.Println(x, "is even3")
		return
	}
	fmt.Println(x, " is odd3")
}

func main() {
	isEven(45)
	isEven2(34)
	isEven3(50)
}

// 45  is odd
// 34 is even2
// 50 is even3
